#!/usr/bin/env python3
#
# Version: 1.0.0
#
# Changelog:
#  1.0.0 - 09.05.19: First release
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

import argparse


PARITYCOLUMNS   = 6      # Number of parity-columns in the generator matrix
SIGNALLINELIMIT = 5*7    # Limit of signal lines


def column(matrix, i):
    return [row[i] for row in matrix]

cli = argparse.ArgumentParser(description="Generate VHDL code for Hamming Encoding")
cli.add_argument("g", type=str, action="store",
    help="Path where the Generator Matrix is stored at (CSV encoded).")
args = cli.parse_args()



if __name__ == "__main__":


    # 1.: Get data from CSV file
    # 1.1 Read Generator Matrix G
    with open(args.g) as f:
        G = f.read().splitlines()

    # 1.2 Separate Parity Matrix P
    P = []  # Parity part of the Generator Matrix
    for row in G:
        columns = row.split(",")[:PARITYCOLUMNS]
        P.append(columns)


    # 2.: Generate list of inputs for xor operations of each parity bit
    # 2.1 Create empty inputs list
    inputs = []
    for column in P[0]:
        inputs.append([])

    # 2.2 Fill inputs list with signal numbers
    #      P[i] = 0 -> no input
    #      P[i] = 1 -> signal(i) is input
    for signal, row in enumerate(P):
        # Do not add more signal lines than available for the data to encode
        if signal >= SIGNALLINELIMIT:
            break

        for column, bit in enumerate(row):
            if bit == "1":
                inputs[column].append(signal)


    # 3.: Generate VHDL code
    for paritybit, _ in enumerate(P[0]):
        print("p%i \033[1;33m<=\033[0m " % (paritybit), end="")
        for pos, signr in enumerate(inputs[paritybit]):
            if pos > 0:
                print(" \033[1;33mxor\033[0m ", end="")
            print("counter\033[1;31m(\033[1;35m%i\033[1;31m)\033[0m"%(signr), end="");
        print("\033[1;31m;\033[0m")


# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

