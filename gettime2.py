#!/usr/bin/env python3
#
# Version: 2.0.0
#
# Changelog:
#  2.0.0 - 09.03.19: First release
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

import sys
import os
import time
import argparse
import binascii
import datetime

try:
    from serial import *
except:
    print("\033[1;31mModule \033[1;37mpyserial \033[1;31mmissing!")
    print("\033[1;34m Try \033[1;36mpip\033[1;30m3\033[1;36m install pyserial\033[1;34m as root to install it.\033[1;30m")
    exit(1)


cli = argparse.ArgumentParser(
    description="Reads time measurement values sent by TMU 2.0.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

cli.add_argument("-s", "--silent", default=False, action="store_true",
    help="Do not show progress")
cli.add_argument("datadevice", type=str, action="store",
    help="Path to the serial communication device for reading measurement data.")
cli.add_argument("n", type=str, action="store",
    help="Number of measurements.")
cli.add_argument("outfile", type=str, action="store",
    help="Path where the measured data will be stored.")


# Generated Check Matrix for Hamming-Decoding
# This matrix must be compatible with the Generator Matrix used in the TMU
# This matrix is modified in the following ways:
#  - Unit matrix for parity bits removed
#  - Rest shorted to 35 bit since there will never be more bits received
CheckMatrix = [
    [1,0,0,0,0,1,1,0,0,0,1,0,1,0,0,1,1,1,1,0,1,0,0,0,1,1,1,0,0,1,0,0,1,0,1],
    [1,1,0,0,0,1,0,1,0,0,1,1,1,1,0,1,0,0,0,1,1,1,0,0,1,0,0,1,0,1,1,0,1,1,1],
    [0,1,1,0,0,0,1,0,1,0,0,1,1,1,1,0,1,0,0,0,1,1,1,0,0,1,0,0,1,0,1,1,0,1,1],
    [0,0,1,1,0,0,0,1,0,1,0,0,1,1,1,1,0,1,0,0,0,1,1,1,0,0,1,0,0,1,0,1,1,0,1],
    [0,0,0,1,1,0,0,0,1,0,1,0,0,1,1,1,1,0,1,0,0,0,1,1,1,0,0,1,0,0,1,0,1,1,0],
    [0,0,0,0,1,1,0,0,0,1,0,1,0,0,1,1,1,1,0,1,0,0,0,1,1,1,0,0,1,0,0,1,0,1,1]
]


class Output(object):
    def __init__(self):
        pass


    def PrintError(self, verbose, message, comment=None):
        if not verbose:
            return

        classname = self.__class__.__name__
        if comment:
            print("\033[1;34m%s\033[1;36m: \033[1;31m%s \033[1;30m(%s)\033[0m"%(classname, message, comment))
        else:
            print("\033[1;34m%s\033[1;36m: \033[1;31m%s\033[0m"%(classname, message))
        return None


    def PrintWarning(self, verbose, message):
        if not verbose:
            return

        classname = self.__class__.__name__
        print("\033[1;34m%s\033[1;36m: \033[1;33m%s\033[0m"%(classname, message))
        return None



class Header(Output):
    def __init__(self, headerbyte):
        Output.__init__(self)

        if type(headerbyte) != bytes:
            raise TypeError("Header-Byte should be of type bytes as returned by uart.read(1)!")
        header = int.from_bytes(headerbyte, byteorder="little", signed=False)

        self.sync     = (header >> 7) & 1
        self.parity   = (header >> 6) & 1
        self.zero     = (header >> 5) & 1
        self.overflow = (header >> 4) & 1
        self.tmcerror = (header >> 3) & 1
        self.size     = (header >> 0) & 7

    def isHeader(self):
        if self.sync == 1:
            return True
        else:
            return False


    def isValid(self, verbose=False):
        if self.sync != 1:
            self.PrintError(verbose, "Invalid Sync-Bit!", "1 expected, 0 received")
            return False

        if self.zero != 0:
            self.PrintError(verbose, "Invalid Zero-Bit!", "0 expected, 1 received")
            return False

        if self.CalculateParity() != 0:
            self.PrintError(verbose, "Parity check failed!")
            return False

        if self.size == 0 or self.size > 5:
            self.PrintError(verbose, "Size-Bits out of range!", "%i not in [1..5]!"%(self.size))
            return False
        return True


    def CalculateParity(self):
        parity  = self.parity
        parity ^= self.overflow
        parity ^= self.tmcerror
        parity ^= (self.size >> 0) & 1
        parity ^= (self.size >> 1) & 1
        parity ^= (self.size >> 2) & 1
        return parity


    def hasErrorFlags(self, verbose=False):
        if self.overflow == 1:
            self.PrintWarning(verbose, "Overflow-Bit is set!")
            return True

        if self.tmcerror == 1:
            self.PrintWarning(verbose, "TMC-Error-Bit is set!")
            return True

        return False


    def GetDataSize(self):
        return self.size



class ParityByte(Output):
    def __init__(self, paritybyte):
        Output.__init__(self)

        if type(paritybyte) != bytes:
            raise TypeError("Header-Byte should be of type bytes as returned by uart.read(1)!")
        data = int.from_bytes(paritybyte, byteorder="little", signed=False)

        self.sync     = (data >> 7) & 0x01
        self.parity   = (data >> 6) & 0x01  # parity bit for this byte
        self.parities = (data >> 0) & 0x3F  # parities for the data bytes following


    def isValid(self, verbose=False):
        if self.sync != 0:
            self.PrintError(verbose, "Invalid Sync-Bit!", "0 expected, 1 received")
            return False

        if self.CalculateParity() != 0:
            self.PrintError(verbose, "Parity check failed!")
            return False
        return True


    def CalculateParity(self):
        parity  = self.parity
        parity ^= (self.parities >> 0) & 1
        parity ^= (self.parities >> 1) & 1
        parity ^= (self.parities >> 2) & 1
        parity ^= (self.parities >> 3) & 1
        parity ^= (self.parities >> 4) & 1
        parity ^= (self.parities >> 5) & 1
        return parity


    def GetParities(self):
        return self.parities

    def GetParity(self, index):
        return (self.parities >> index) & 1


class DataByte(Output):
    def __init__(self, databyte):
        Output.__init__(self)

        if type(databyte) != bytes:
            raise TypeError("Header-Byte should be of type bytes as returned by uart.read(1)!")
        data = int.from_bytes(databyte, byteorder="little", signed=False)

        self.sync   = (data >> 7) & 0x01
        self.data   = (data >> 0) & 0x7F


    def isValid(self, verbose=False):
        if self.sync != 0:
            self.PrintError(verbose, "Invalid Sync-Bit!", "0 expected, 1 received")
            return False
        return True


    def GetData(self):
        return self.data



class Packet(Output):
    def __init__(self, uart):
        Output.__init__(self)

        if type(uart) != Serial:
            raise TypeError("uart must be an instance of class Serial!")

        self.header = None  # header of the packet (of type Header)
        self.parity = None  # parity byte of the packet (of type ParityByte)
        self.data   = []    # data bytes of the packet (of type DataByte)
        self.time   = 0     # decoded time after calling DecodePacket (of type int)


    def ReadByte(self):
        try:
            byte = uart.read(1)
        except Exception as e:
            self.PrintError(True, "Reading byte from UART failed!", "Raising Exception!")
            raise(e)
        return byte


    def Receive(self):
        """
        Tries to read a new packet.
        If something goes wrong, it returns False,
        Otherwise True gets returned.
        """
        # 1.: Read Header
        while True:
            byte = self.ReadByte()
            self.header = Header(byte)
            if self.header.isHeader():
                break

        if not self.header.isValid(verbose=True):
            return False
        if self.header.hasErrorFlags(verbose=True):
            return False

        # 2.: Read Parity-Byte
        byte = self.ReadByte()
        self.parity = ParityByte(byte)
        if not self.parity.isValid(verbose=True):
            return False

        # 3.: Read Data bytes
        self.data = []
        for i in range(self.header.GetDataSize()):
            byte = self.ReadByte()
            data = DataByte(byte)
            if not data.isValid(verbose=True):
                return False
            self.data.append(data)

        return True


    def Decode(self):
        """
        Returns true on success
        In case of an decoding/hamming check error, False gets returned.
        """
        # 1.: Get raw data from Data Packet
        rawdata = [byte.GetData() for byte in self.data]
        # Order list by significant of the byte - so last received byte at the begin because it is the MSB
        rawdata.reverse()
        self.time = 0
        for chunk in rawdata:
            self.time <<= 7;
            self.time  |= chunk;

        # 2.: Apply Hamming-Code check matrix
        for index, checkrow in enumerate(CheckMatrix):
            p = self.parity.GetParity(index)

            # for each of the 35 time-counter bits:
            for i in range(35):
                p ^= (self.time >> i) & checkrow[i]

            if p != 0:
                self.PrintError(True, "Hamming decoding revealed a transmission error!", "First invalid parity at %i"%(index))
                return False
        return True


    def GetTime(self):
        return self.time




if __name__ == '__main__':

    # handle command line arguments
    args        = cli.parse_args()
    DATADEVICE  = args.datadevice
    OUTFILE     = args.outfile
    N           = int(args.n)
    SILENT      = bool(args.silent)

    # Open terminal device
    try:
        uart = Serial(
            port    = DATADEVICE,
            baudrate= 115200,
            xonxoff = 0,
            rtscts  = 0,
            interCharTimeout=None
        )
    except Exception as e:
        print("\033[1;31mAccessing \033[1;37m" + DEVICE + " \033[1;31mfailed with the following excpetion:\033[0m")
        print(e)
        exit(1)


    try:
        outfile = open(OUTFILE, "w")
    except Exception as e:
        print("\033[1;31mAccessing \033[1;37m" + OUTFILE + " \033[1;31mfailed with the following excpetion:\033[0m")
        print(e)
        exit(1)


    fails  = 0
    packet = Packet(uart) 
    i      = 0
    tries  = 0
    t_begin= datetime.datetime.now()
    while i < N:
        t_start = datetime.datetime.now()
        tries += 1

        # Check consecutive-fails-state
        if fails >= 50:
            print("\033[1;33mUnexpected high error rate of %i times in a row!\033[0m"%(fails))
            print("\033[1;31mExiting...\033[0m")
            uart.close()
            outfile.close()
            exit(2)

        # Show progress
        if not SILENT:
            print("\033[1;30mGetTime: \033[1;34m[\033[0;36m%2i%%\033[1;30m %7i\033[1;34m]\033[0m"%(int((i*100)/N), i), end=" ")

        # Read Packet
        success = packet.Receive()
        if not success:
            fails += 1
            continue

        # Decode Packet
        success = packet.Decode()
        if not success:
            fails += 1
            continue

        # Get time value
        delay = packet.GetTime()

        # Store time in file
        outfile.write(str(delay)+"\n")
        if not SILENT:
            print("\033[1;35m%7i\033[0m"%(delay), end=" ")

        # Update states
        i     += 1  # Update number of successful measurement
        fails  = 0  # Reset fail-counter
        t_stop = datetime.datetime.now()
        t_diff = t_stop - t_start

        if not SILENT:
            t_avgdiff = (t_stop - t_begin)
            t_pred = (N - i) * (t_avgdiff / i)
            minutes = int(t_pred.seconds / 60)
            seconds = int(t_pred.seconds) - minutes*60
            print("\033[0;36m%2i:%02i\033[1;30m remaining"%(int(minutes), int(seconds)))


    uart.close()
    outfile.close()
    print("\033[1;36m%i\033[1;34m tries to get \033[1;32m%i valid packets\033[1;34m (\033[1;31m%i invalid packets\033[1;34m)\033[0m"%(tries, i, tries-i))
    print("\033[1;34mData stored at \033[1;36m%s\033[0m"%(OUTFILE))
    print("\033[1;34mTotal measurement time \033[1;36m%s\033[0m"%(str(t_stop - t_begin)))


# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

