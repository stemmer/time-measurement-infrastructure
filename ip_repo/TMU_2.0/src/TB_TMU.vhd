----------------------------------------------------------------------------------
-- Company: Carl von Ossietzky Universität Oldenburg
-- Engineer: Ralf Stemmer
-- 
-- Create Date: 09.05.2019 11:31:24
-- Design Name: Test Bench for Timing Measurement Unit
-- Module Name: Test Bench for TMU - Behavioral
-- Project Name: Timing Measurement Infrastructure
-- Target Devices: ZC702
-- Tool Versions: 2018.3
-- Description: Providing the actual time measurement via counter
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity tb_tmu is
--  Port ( );
end tb_tmu;

architecture Behavioral of tb_tmu is
    constant PERIOD : time := 10 ns;
    signal clk      : std_logic;
    signal rst      : std_logic;
    signal test     : integer;
    signal one      : std_logic := '1';

    component tmu
        port (
            sysclk  : in    std_logic;
            sysnrst : in    std_logic;
            trigger : in    std_logic;      -- to start (1) and stop (0) the counter
            tmcerror: in    std_logic_vector(1 downto 0);   -- propagated error codes from the Time Measurement Controller
            txd     : out   std_logic;      -- The TxD signal from the UART component
            ready   : out   std_logic       -- Ready to get triggered - data were send at that time
        );
    end component;

    signal trigger  : std_logic;
    signal tmcerror : std_logic_vector(1 downto 0) := "00";
    signal txd      : std_logic;
    signal ready    : std_logic;


begin

    DUT : tmu
    port map(
        sysclk  => clk,
        sysnrst => rst,
        trigger => trigger,
        tmcerror => tmcerror,
        txd     => txd,
        ready   => ready
    );


    CLOCK : process
    begin
        clk <= '1';
        wait for PERIOD / 2;
        clk <= '0';
        wait for PERIOD / 2;
    end process;


    SIMULATION : process
    begin
        test    <= 1;   -- reset
        trigger <= '0';
        rst     <= '0';
        wait for PERIOD * 2;
 
        rst     <= '1';
        wait for PERIOD * 1;
 
        test    <= 2;   -- Measure 1 cycle
        trigger <= '1';
        wait for PERIOD;
        trigger <= '0';
        wait for 300 us; -- wait until data was sent
 
        test    <= 3;   -- Measure 10 cycle
        trigger <= '1';
        wait for PERIOD * 10;
        trigger <= '0';
        wait for 300 us; -- wait until data was sent
 
        wait;
    end process;



end Behavioral;
