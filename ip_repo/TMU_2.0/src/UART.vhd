----------------------------------------------------------------------------------
-- Company: Carl von Ossietzky Universität Oldenburg
-- Engineer: Ralf Stemmer
-- 
-- Create Date: 03.05.2019 11:31:24
-- Design Name: Timing Measurement Unit
-- Module Name: UART - Behavioral
-- Project Name: Timing Measurement Infrastructure
-- Target Devices: ZC702
-- Tool Versions: 2018.3
-- Description: Providing the actual time measurement via counter
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;



entity uart is
    generic (
            CLOCKFREQUENCY  : integer := 100_000_000;
            BAUDRATE        : integer := 115200
    );
    port (
        sysclk  : in    std_logic;
        sysnrst : in    std_logic;
        txd     : out   std_logic;
        packet  : in    std_logic_vector((7 * 8 - 1) downto 0); -- 5 bytes each with 8 bits + header + parity
        send    : in    std_logic; -- start to send the packet
        ready   : out   std_logic  -- packet sent
    );
end uart;



architecture Behavioral of uart is
    constant MAXBYTES      : integer := 7;  -- max number of bytes that will send inclusive header byte. (8bit/byte)
    constant SYSCLKSPERBIT : integer := (CLOCKFREQUENCY / BAUDRATE) - 1; -- -1 To get the exact frequency 

    type txstates   is (IDLE, STARTBIT, DATABIT, STOPBIT1, STOPBIT2);
    type uartstates is (IDLE, STARTTX, SENDING);

    signal currentuartstate, nextuartstate : uartstates;
    signal currenttxstate, nexttxstate : txstates;
    signal bitindex     : integer range 0 to (MAXBYTES * 8); -- (MAXBYTES * 8 - 1); -- \_ "overflow" required for comparison
    signal nextbitindex : integer range 0 to (MAXBYTES * 8); -- (MAXBYTES * 8 - 1); -- /  in PacketSent(…)
    signal uartcounter  : integer range 0 to SYSCLKSPERBIT; -- ring counter that is 0 with the frequency of the baudrate
    signal uartclock    : std_logic;
    signal start        : std_logic;    -- 1 when the uart is sending a packet



    -- The amount of data bytes is encoded in the first 2 bits: 
    --  001: 1
    --  010: 2
    --  011: 3
    --  100: 4
    --  101: 5
    -- The bytes already send can be determined by the bit index
    -- keep in mind that there is an additional header byte and a parity byte!
    -- So there is an offset of 2·8 sent bits
    function PacketSent(
        sentbits  : in integer;    -- bit counter
        databytes : in std_logic_vector(2 downto 0) -- first two bits of the headers
    )
        return boolean is
        variable retval: boolean;
    begin
        retval := false;
           if databytes = std_logic_vector(to_unsigned(1, databytes'length)) and sentbits = 3*8 then
            retval := true;
        elsif databytes = std_logic_vector(to_unsigned(2, databytes'length)) and sentbits = 4*8 then
            retval := true;
        elsif databytes = std_logic_vector(to_unsigned(3, databytes'length)) and sentbits = 5*8 then
            retval := true;
        elsif databytes = std_logic_vector(to_unsigned(4, databytes'length)) and sentbits = 6*8 then
            retval := true;
        elsif databytes = std_logic_vector(to_unsigned(5, databytes'length)) and sentbits = 7*8 then
            retval := true;
        end if;
        --   if databytes = std_logic_vector(1, databytes'length) and sentbits = 3*8 then
        --    rerval := true
        --elsif databytes = std_logic_vector(2, databytes'length) and sentbits = 4*8 then
        --    rerval := true
        --elsif databytes = std_logic_vector(3, databytes'length) and sentbits = 5*8 then
        --    rerval := true
        --elsif databytes = std_logic_vector(4, databytes'length) and sentbits = 6*8 then
        --    rerval := true
        --elsif databytes = std_logic_vector(5, databytes'length) and sentbits = 7*8 then
        --    rerval := true
        --end if;
        return retval;

    end function PacketSent;


begin


    REGISTER_UARTSTATE: process (sysclk, sysnrst)
    begin
        if sysnrst = '0' then
            currentuartstate <= IDLE;
        elsif rising_edge(sysclk) then
            currentuartstate <= nextuartstate;
        end if;
    end process;



    REGISTER_TXSTATE: process (sysclk, sysnrst, uartclock)
    begin
        if sysnrst = '0' then
            currenttxstate <= IDLE;
        elsif rising_edge(uartclock) then
            currenttxstate <= nexttxstate;
        end if;
    end process;



    REGISTER_TXD: process (sysclk, sysnrst)
    begin
        if sysnrst = '0' then
            txd <= '1';
        elsif rising_edge(sysclk) then
            case currenttxstate is
                when STARTBIT => txd <= '0';
                when STOPBIT1 => txd <= '1';
                when STOPBIT2 => txd <= '1';
                when DATABIT  => txd <= packet(bitindex);
                when others   => txd <= '1';
            end case;
        end if;
    end process;



    REGISTER_BITINDEX: process (sysclk, sysnrst, uartclock, currenttxstate)
    begin
        if sysnrst = '0' then
            bitindex <= 0;
        elsif rising_edge(uartclock) then
            bitindex <= nextbitindex;
        end if;
    end process;



    CLOCKDIVIDER_UARTCLK: process (sysclk, sysnrst)
    begin
        if sysnrst = '0' then
            uartcounter <= 0;
            uartclock   <= '0';
        elsif rising_edge(sysclk) then
            -- update clock signal
            if uartcounter < SYSCLKSPERBIT/2 then
                uartclock <= '1';
            else
                uartclock <= '0';
            end if;

            -- update counter
            if uartcounter < SYSCLKSPERBIT then
                uartcounter <= uartcounter + 1;
            else
                uartcounter <= 0;
            end if;
        end if;
    end process;



    UART_CONTROLLER: process (currentuartstate, send, currenttxstate)
    begin
        case currentuartstate is
            when IDLE =>
                ready <= '1';
                start <= '0'; 
                if send = '1' then
                    nextuartstate <= STARTTX;
                else
                    nextuartstate <= IDLE;
                end if;

            when STARTTX =>
                ready <= '0';
                start <= '1'; 
                -- wait until TX controller leaves IDLE state
                if currenttxstate /= IDLE then
                    nextuartstate <= SENDING;
                else                  
                    nextuartstate <= STARTTX;
                end if;
                
            when SENDING =>
                ready <= '0';
                start <= '0'; 
                if send = '0' and currenttxstate = IDLE then
                    nextuartstate <= IDLE;
                else
                    nextuartstate <= SENDING;
                end if;
            end case;
    end process;



    TX_CONTROLLER: process (currenttxstate, start, bitindex)
    begin
        case currenttxstate is
            when IDLE =>
                nextbitindex <= 0;
                -- Check if a new packet shall be send
                if start = '1' then
                    nexttxstate <= STARTBIT;
                else
                    nexttxstate <= IDLE;
                end if;

            when STARTBIT =>
                nextbitindex <= bitindex;
                nexttxstate <= DATABIT;

            when DATABIT =>
                nextbitindex <= bitindex + 1;
                -- Check if a complete byte was sent.
                -- better, but still ugly - TODO: use std_logic_vector and compare bitindex(2 downto 0) = '111'
                if bitindex = 7 
                or bitindex = 15 
                or bitindex = 23 
                or bitindex = 31 
                or bitindex = 39 
                or bitindex = 47 
                or bitindex = 55
                or bitindex = 63 then 
                    nexttxstate <= STOPBIT1;
                else
                    nexttxstate <= DATABIT;
                end if;
                
            when STOPBIT1 =>
                nextbitindex <= bitindex;
                nexttxstate  <= STOPBIT2;
                        
            when STOPBIT2 =>
                nextbitindex <= bitindex;
                -- Check if the complete packet was sent
                -- The among of data bytes is encoded in the first 3 bits
                -- The bytes already send can be determined by the bit index
                if PacketSent(bitindex, packet(2 downto 0)) then
                    nexttxstate <= IDLE;
                else
                    nexttxstate <= STARTBIT;
                end if;
        end case;
    end process;

end Behavioral;
-- vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
