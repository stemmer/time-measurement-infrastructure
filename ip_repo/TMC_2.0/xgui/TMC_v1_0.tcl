# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "numstarts" -parent ${Page_0}
  ipgui::add_param $IPINST -name "numstops" -parent ${Page_0}


}

proc update_PARAM_VALUE.numstarts { PARAM_VALUE.numstarts } {
	# Procedure called to update numstarts when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.numstarts { PARAM_VALUE.numstarts } {
	# Procedure called to validate numstarts
	return true
}

proc update_PARAM_VALUE.numstops { PARAM_VALUE.numstops } {
	# Procedure called to update numstops when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.numstops { PARAM_VALUE.numstops } {
	# Procedure called to validate numstops
	return true
}


proc update_MODELPARAM_VALUE.numstarts { MODELPARAM_VALUE.numstarts PARAM_VALUE.numstarts } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.numstarts}] ${MODELPARAM_VALUE.numstarts}
}

proc update_MODELPARAM_VALUE.numstops { MODELPARAM_VALUE.numstops PARAM_VALUE.numstops } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.numstops}] ${MODELPARAM_VALUE.numstops}
}

