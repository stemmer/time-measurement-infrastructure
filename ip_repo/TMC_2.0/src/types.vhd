library ieee;
use ieee.std_logic_1164.all;

package types is
    type TMBIN_ARRAY_type is array (integer range <>) of std_logic_vector (1 downto 0);

    function or_reduce (input : TMBIN_ARRAY_type) return std_logic_vector;
end types;



package body types is

function or_reduce(input : TMBIN_ARRAY_type) return std_logic_vector is
    variable retval : std_logic_vector(1 downto 0) := (others => '0');
begin
    for i in input'range loop
        retval := retval or input(i);
    end loop;

    return retval;
end function or_reduce;

end types;

-- vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
