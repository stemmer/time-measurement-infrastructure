----------------------------------------------------------------------------------
-- Company: Carl von Ossietzky Universität Oldenburg
-- Engineer: Ralf Stemmer
-- 
-- Create Date: 03.05.2019 11:31:24
-- Design Name: Timing Measurement Controller
-- Module Name: TMC - Behavioral
-- Project Name: Timing Measurement Infrastructure
-- Target Devices: ZC702
-- Tool Versions: 2018.3
-- Description: Collecting all start and stop signals from the TMBs and trigger the TMU
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

library work;
use work.types.all;


entity TMC is
    generic (
        numstarts : integer := 1;
        numstops  : integer := 1
    );
    port (
        sysclk  : in    std_logic;
        sysnrst : in    std_logic;
        starts  : in    std_logic_vector((numstarts - 1) downto 0);
        stops   : in    std_logic_vector((numstops  - 1) downto 0);
        trigger : out   std_logic;
        errors  : out   std_logic_vector(1 downto 0);   -- [0, 0] Not yet needed anymore
        ready   : in    std_logic
    );
end TMC;

architecture Behavioral of TMC is

    signal combinedinput: std_logic_vector(1 downto 0);
    signal start        : std_logic;
    signal stop         : std_logic;
    signal nextstart    : std_logic;
    signal nextstop     : std_logic;

    signal nextstartid  : std_logic_vector(3 downto 0);
    signal startid      : std_logic_vector(3 downto 0);
    signal nextstopid   : std_logic_vector(3 downto 0);
    signal stopid       : std_logic_vector(3 downto 0);
    signal nextiterid   : std_logic_vector(3 downto 0);
    signal iterid       : std_logic_vector(3 downto 0);

    signal tmutrigger   : std_logic;
    signal tmberror     : std_logic;    -- protocol error       \_ no more in use for v2.0 (always '0')
    signal tmcerror     : std_logic;    -- double start error   /
    signal nexttmberror : std_logic;
    signal nexttmcerror : std_logic;
    signal errorreset   : std_logic;    -- when '1', then tmberror and tmcerror will be forced '0'

    type states is (IDLE, MEASURE, SENDING, ERRORSTATE, RESETERRORS);
    signal currentstate, nextstate : states;
begin

    nextstart     <= or_reduce(starts);
    nextstop      <= or_reduce(stops);

    nexttmberror <= tmberror;
    nexttmcerror <= tmcerror;

    errors(0) <= tmberror;
    errors(1) <= tmcerror;
    trigger   <= tmutrigger;


    COUNTER_STARTID: process (nextstart, startid)
    begin
        nextstartid <= startid;
        if rising_edge(nextstart) then
            nextstartid <= std_logic_vector(unsigned(startid) + 1);
        end if;
    end process;

    REGISTER_START: process (sysclk, sysnrst, nextstartid, nextstart)
    begin
        if sysnrst = '0' then
            startid <= (others => '0');
            start   <= '0';
        elsif rising_edge(sysclk) then
            startid <= nextstartid;
            start   <= nextstart;
        end if;
    end process;



    COUNTER_STOPID: process (nextstop, stopid)
    begin
        nextstopid <= stopid;
        if rising_edge(nextstop) then
            nextstopid <= std_logic_vector(unsigned(stopid) + 1);
        end if;
    end process;

    REGISTER_STOP: process (sysclk, sysnrst, nextstopid, nextstop)
    begin
        if sysnrst = '0' then
            stopid <= (others => '0');
            stop   <= '0';
        elsif rising_edge(sysclk) then
            stopid <= nextstopid;
            stop   <= nextstop;
        end if;
    end process;



    REGISTER_ERRORS: process (sysclk, sysnrst, errorreset)
    begin
        if errorreset = '1' or sysnrst = '0' then
            tmberror <= '0';
            tmcerror <= '0';
        elsif rising_edge(sysclk) then
            tmberror <= nexttmberror;
            tmcerror <= nexttmcerror;
        end if;
    end process;



    REGISTER_STATE: process (sysclk, sysnrst)
    begin
        if sysnrst = '0' then
            currentstate <= SENDING;
        elsif rising_edge(sysclk) then
            currentstate <= nextstate;
        end if;
    end process;

    REGISTER_ITERID: process (sysclk, sysnrst)
    begin
        if sysnrst = '0' then
            iterid <= (others => '0');
        elsif rising_edge(sysclk) then
            iterid <= nextiterid;
        end if;
    end process;



    CONTROLLER : process (sysclk, sysnrst, currentstate, start, stop, tmberror, tmcerror, ready)
    begin
        case currentstate is
            when IDLE =>
                errorreset <= '0';
                tmutrigger <= '0';
                nextiterid <= startid;

                if start = '1' then
                    nextstate <= MEASURE;
                elsif tmberror = '1' or tmcerror = '1' then
                    nextstate <= ERRORSTATE;
                else
                    nextstate <= IDLE;
                end if;


            when MEASURE =>
                errorreset <= '0';
                tmutrigger <= '1';
                nextiterid <= iterid;

                if stop = '1' and iterid = stopid then
                    nextstate <= SENDING;
                elsif tmberror = '1' or tmcerror = '1' then
                    nextstate <= ERRORSTATE;
                else
                    nextstate <= MEASURE;
                end if;


            when ERRORSTATE =>
                errorreset <= '0';
                tmutrigger <= '1';
                nextiterid <= iterid;

                nextstate <= SENDING;


            when SENDING =>
                errorreset <= '0';
                tmutrigger <= '0';
                nextiterid <= iterid;

                if ready = '1' then
                    nextstate <= RESETERRORS;
                else
                    nextstate <= SENDING;
                end if;


            when RESETERRORS =>
                errorreset <= '1';
                tmutrigger <= '0';
                nextiterid <= iterid;

                nextstate <= IDLE;


        end case;
    end process;

end Behavioral;

-- vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
