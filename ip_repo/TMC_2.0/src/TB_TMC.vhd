library ieee;
use ieee.std_logic_1164.all;

library work;
use work.types.all;


entity TB_TMC is
end TB_TMC;

architecture Behavioral of tb_tmc is

    constant PERIOD : time := 10 ns;
    signal clk      : std_logic;
    signal rst      : std_logic;
    signal one      : std_logic := '1';

    component TMC
        generic (
            numstarts : integer := 1;
            numstops  : integer := 1
        );
        port (
            sysclk  : in    std_logic;
            sysnrst : in    std_logic;
            starts  : in    std_logic_vector((numstarts - 1) downto 0);
            stops   : in    std_logic_vector((numstops  - 1) downto 0);
            trigger : out   std_logic;
            errors  : out   std_logic_vector(1 downto 0);   -- [Protocol Error (tmbxin.error), Double Start Error]
            ready   : in    std_logic
        );
    end component;


    signal trigger  : std_logic;
    signal errors   : std_logic_vector(1 downto 0);
    signal test     : integer;
    signal starts   : std_logic_vector(3 downto 0);
    signal stops    : std_logic_vector(3 downto 0);

begin

    DUT : tmc
    generic map(
        numstarts   => 4,
        numstops    => 4
               )
    port map(
        sysclk  => clk,
        sysnrst => rst,
        starts  => starts,
        stops   => stops,
        trigger => trigger,
        errors  => errors,
        ready   => one  -- be always ready
    );
    --generic map ( numinputs => 4)
        --tmbin   => (tmb0in, tmb1in, tmb2in, tmb3in),

    CLOCK : process
    begin
        clk <= '1';
        wait for PERIOD / 2;
        clk <= '0';
        wait for PERIOD / 2;
    end process;


    SIMULATION : process
    begin
        test    <= 1;   -- reset
        starts  <= "0000";
        stops   <= "0000";
        rst     <= '0';
        wait for PERIOD * 2;

        rst     <= '1';
        wait for PERIOD * 1;


        test    <= 0;   -- WAITING 
        wait for PERIOD * 20;

        test    <= 2;   -- bug-case

        -- Start 1
        starts  <= "0001";
        stops   <= "0000";
        wait for PERIOD * 1;
        starts  <= "0000";
        stops   <= "0000";
        
        wait for PERIOD * 5;
        
        -- Start 2
        starts  <= "0001";
        stops   <= "0000";
        wait for PERIOD * 1;
        starts  <= "0000";
        stops   <= "0000";
        
        wait for PERIOD * 5;
        
        -- Stop 1
        starts  <= "0000";
        stops   <= "0010";
        wait for PERIOD * 1;
        starts  <= "0000";
        stops   <= "0000";
        
        wait for PERIOD * 5;
        
        -- Start 3
        starts  <= "0001";
        stops   <= "0000";
        wait for PERIOD * 1;
        starts  <= "0000";
        stops   <= "0000";
        
        wait for PERIOD * 5;
       
        -- Stop 2
        starts  <= "0000";
        stops   <= "0010";
        wait for PERIOD * 1;
        starts  <= "0000";
        stops   <= "0000";
        
        wait for PERIOD * 5;
        
        -- Start 4
        starts  <= "0001";
        stops   <= "0000";
        wait for PERIOD * 1;
        starts  <= "0000";
        stops   <= "0000";
        
        wait for PERIOD * 5;
        
        -- Stop 3
        starts  <= "0000";
        stops   <= "0010";
        wait for PERIOD * 1;
        starts  <= "0000";
        stops   <= "0000";
        
        wait for PERIOD * 5;



        test    <= 0;   -- WAITING 
        wait for PERIOD * 10;


        test    <= 1;   -- reset
        starts  <= "0000";
        stops   <= "0000";
        rst     <= '0';
        wait for PERIOD * 1;
        rst     <= '1';
        wait for PERIOD * 1;
        test    <= 0;   -- WAITING 
        wait for PERIOD * 5;


        test    <= 3;   -- measure 10 clk
        -- Start
        starts  <= "0001";
        stops   <= "0000";
        wait for PERIOD * 1;
        starts  <= "0000";
        stops   <= "0000";
        wait for PERIOD * 9;
        -- Stop
        starts  <= "0000";
        stops   <= "1000";
        wait for PERIOD * 1;
        starts  <= "0000";
        stops   <= "0000";
        wait for PERIOD * 5;


        test    <= 0;   -- WAITING 
        wait for PERIOD * 10;


        test    <= 0;   -- WAITING 
        wait;
    end process;

end Behavioral;

-- vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

