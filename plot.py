#!/usr/bin/env python3
#
# Version: 1.0.0
#
# Changelog:
#  1.0.0 - 11.03.19: First release
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

import numpy as np
import math 
import matplotlib.pyplot as pyplot
import csv
import argparse
import array



def column(matrix, i):
    return [row[i] for row in matrix]

cli = argparse.ArgumentParser(description='Plot Graph from File')
cli.add_argument("infile", type=str, action="store",
    help="Path where the measured data will be read from.")
args = cli.parse_args()

if __name__ == '__main__':
    # read vector of data
    with open(args.infile) as f:
        lines = f.read().splitlines()

    data   = [int(line) for line in lines]
    print("\033[1;34mmin: \033[1;35m %5i"%(min(data)))
    print("\033[1;34mmax: \033[1;35m %5i"%(max(data)))
    print("\033[1;34mavg: \033[1;35m %5f"%(sum(data)/float(len(data))))
    npdata = np.asarray(data)
    print("\033[1;34mvar: \033[1;35m %5f"%(np.var(npdata)))

    fig = pyplot.figure()
    fig.set_size_inches(18.5, 10.5)
    fig.suptitle("time in cycles", fontsize=11)

    # time over time
    time = fig.add_subplot(1,2,1, label="time")
    time.plot(np.arange(len(npdata)), npdata,
              color="C0", markersize = 1, marker = ".", linestyle = "")
    time.set_xlabel("Measurement", color="C0")
    time.set_ylabel("Time", color="C0")
    time.tick_params(axis="x", colors="C0")
    time.tick_params(axis="y", colors="C0")
    pyplot.grid(True)

    # histogram
    hist = fig.add_subplot(1,2,2, label="hist")
    hist.hist(npdata, 100, density=0, color="C1")
    hist.set_xlabel("Time", color="C1")
    hist.set_ylabel("Occurence", color="C1")
    hist.tick_params(axis="x", colors="C1")
    hist.tick_params(axis="y", colors="C1")
    pyplot.grid(True)

    pyplot.show()

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

